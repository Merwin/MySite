'use strict';

var bucketValues = [27.5, 29, 30.5, 32.7, 35,
  36.71, 41.21, 43.65, 46, 49.00, 56, 58,
  61.74, 65.41, 69, 73.42, 78, 82.41, 87.31, 93, 98,
  104, 110, 117, 123.74, 130.81, 139, 146.83, 156,
  164.81, 174.61, 185, 196.00, 208, 220.00, 233, 246.94,
  261.63, 293.66, 311, 329.63, 349.23, 370, 392.00, 415,
  440, 466, 493.88, 523.25, 554, 587.33, 622, 659.26,
  698.46, 740, 783.99, 831, 880, 932, 987.77, 1046.5, 1109,
  1174.70, 1245, 1318.5, 1396.9, 1480, 1568.0, 1661, 1760,
  1865, 1975.5, 2093, 2217, 2349.30, 2489, 2637, 2793.80, 2960,
  3126, 3322, 3520, 3729, 3951.10, 4186];

//Given a spectrograph like [35, 37, 3400, 2300];
//Make a grouping of 44100 frequencies at a time and pulse the scale transform
//Every second

//Pseudocode:

//Given freq f --> find where it would be sorted if
//it were to be inserted in the bucketValues list
//then, once find place, determine if diff is
//closer to forwards element or the backward one if
//either exists and add one to that index in the bucketTally list

function updateBucketTally(bucketTally, freq) {
  var lowIndex = 0;
  var highIndex = bucketValues.length - 1;

  while(highIndex - lowIndex > 1) {
    var middleIndex = Math.floor((lowIndex + highIndex) / 2);

    if (freq < bucketValues[middleIndex]) { //Freq is low so toss out the high index values
      highIndex = middleIndex;
    } else { //Freq is high so toss out the low index values
      lowIndex = middleIndex;
    }
  }

  var higherDifference = Math.abs(freq - bucketValues[highIndex]); //Taking absolute value assures it'll work for edge cases where diff is negative.
  var lowerDifference = Math.abs(freq - bucketValues[lowIndex]);

  if (lowerDifference < higherDifference) {
    bucketTally[low] += 1;
  } else {
    bucketTally[highIndex] += 1;
  }

  return bucketTally;

}

function scanBatchOfFrequencies(freqArray) {
  var largeArrayOfTallies = []; //An array that will be full of 44100 length arrays of the buckets that should pulse in each second
  var bucketTally = new Array(bucketValues.length).fill(0);
  for (let i = 0; i < freqArray.length; i += 1) {
    if (i % 44100 == 0 && i != 0) {
      largeArrayOfTallies.push(bucketTally);
      bucketTally = new Array(bucketValues.length).fill(0);
    }
    bucketTally = updateBucketTally(bucketTally);
  }
  return largeArrayOfTallies;
}
