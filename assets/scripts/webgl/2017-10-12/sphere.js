var sphereVertices = [];
var sphereNormals = [];
var sphereIndices = [];
var sphereColors = [];

console.log("Initialized everything for sphereXXX");

(function() {
    var NumPhiBands = 30;
    var NumThetaBands = NumPhiBands/2;

    for (var i = 0; i <= NumThetaBands; ++i) {
        for (var j = 0; j < NumPhiBands; ++j) {
            var theta = (i/NumThetaBands - 0.5)*Math.PI;
            var phi = 2*Math.PI*j/NumPhiBands

            var x = Math.cos(phi)*Math.cos(theta);
            var y = Math.sin(theta);
            var z = Math.sin(phi)*Math.cos(theta);

            sphereVertices.push(x);
            sphereVertices.push(y);
            sphereVertices.push(z);

            if (x < 0.0) {
              sphereColors.push(1.0); //R
            } else if (x >= 0.0) {
              sphereColors.push(0.0);
            }

            if (y < 0.0) {
              sphereColors.push(1.0); //G
            } else if (y >= 0.0) {
              sphereColors.push(0.0);
            }

            if (z < 0.0) {
              sphereColors.push(1.0); //B
            } else {
              sphereColors.push(0.0);
            }

            sphereNormals.push(-x);
            sphereNormals.push(-y);
            sphereNormals.push(-z);

            if (i < NumThetaBands) {
                var i0 = i, i1 = i + 1;
                var j0 = j, j1 = (j + 1) % NumPhiBands;
                sphereIndices.push(i0*NumPhiBands + j0);
                sphereIndices.push(i0*NumPhiBands + j1);
                sphereIndices.push(i1*NumPhiBands + j1);
                sphereIndices.push(i0*NumPhiBands + j0);
                sphereIndices.push(i1*NumPhiBands + j1);
                sphereIndices.push(i1*NumPhiBands + j0);
            }
        }
    }
})();
