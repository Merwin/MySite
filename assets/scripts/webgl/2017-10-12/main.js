/* Created by Mark Erwin */
"use strict";

var aspectRatio = 0;

var vertexSource = `
  precision mediump float;

  attribute vec3 aVertPosition;
  attribute vec3 aVertNormals;
  attribute vec3 aVertColor;

  varying vec3 vWorldPosition;
  varying vec3 vNormal;
  varying vec3 fragColor;

  uniform mat4 mModel;
  uniform mat4 mView;
  uniform mat4 mProj;
  uniform mat4 mInvView;
  uniform mat4 mInvModel;

  void main() {
    vWorldPosition = (mModel * vec4(aVertPosition, 1)).xyz;
    vNormal = (mModel * vec4(aVertNormals, 0)).xyz;
    fragColor = aVertColor;
    gl_Position = mProj * mView * mModel * vec4(aVertPosition, 1.0);
  }
`;

var fragmentSource = `

  //TODO: Add Blinn-Phong shading to scene here

  precision mediump float;

  uniform mat4 mModel;
  uniform mat4 mInvView;
  uniform mat4 mInvModel;

  varying vec3 vWorldPosition;
  varying vec3 vNormal;
  varying vec3 fragColor;

  void main()
  {

    const vec3 lightPosition = vec3(0,1.5,-6);
    const vec3 lightIntensity = vec3(50);
    vec3 ka = 0.3*fragColor;
    vec3 kd = 0.7*fragColor;
    const vec3 ks = vec3(0.4);
    const float alpha = 64.0;
    const vec3 origin = vec3(0);
    vec3 worldCameraPos = (mInvView * vec4(origin, 1)).xyz;
    vec3 pointToCam =  worldCameraPos - vWorldPosition;
    vec3 pointToLight = lightPosition - vWorldPosition;
    float distanceFromLightSquared = dot(pointToLight, pointToLight);

    vec3 normal = normalize(vNormal);
    pointToLight = normalize(pointToLight);
    pointToCam = normalize(pointToCam);
    vec3 halfwayVector = normalize(pointToCam + pointToLight);

    vec3 La = ka*kd;
    vec3 Ld = kd*lightIntensity / distanceFromLightSquared * max(0.0, dot(normal, pointToLight));
    vec3 Ls = ks*lightIntensity / distanceFromLightSquared * pow(max(0.0, dot(normal, halfwayVector)), alpha);

    vec3 totalLight = La + Ld + Ls;

    gl_FragColor = vec4(totalLight, 1.0);
  }
`;

function initializeWebGL() {
  console.log("This is working!");

  var canvas = document.getElementById('webglCanvas');
  aspectRatio = canvas.width / canvas.height;

  var gl = canvas.getContext('webgl');

  if (!gl) {
    console.log("WebGL not supported falling back on experimental");
    gl = canvas.getContext('experimental-webgl');
  }

  if (!gl) {
    alert("Your browser does not support webGL :(")
  }

  gl.clearColor(0.0, 0.0, 0.1, 1.0); //Set the clear color -- shouldn't do anything itself until gl.clear call
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); //Perform clear on both depth and color buffer


  //Enable depth testing and backface culling --> can also do front-face culling if you want to see inside the cube for whatever reason
  gl.enable(gl.DEPTH_TEST);
  gl.enable(gl.CULL_FACE);
  gl.cullFace(gl.BACK);
  gl.frontFace(gl.CCW);
  //WebGL has been initialized!

  return gl;
}

function initializeShader(gl, shaderSource, shaderType) {

  var shader = gl.createShader(shaderType);

  gl.shaderSource(shader, shaderSource); //Set the source code for the shader
  gl.compileShader(shader); //Compile the shader!

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    console.log("Error compiling vertex shader!", gl.getShaderInfoLog(shader));
    return null;
  }
  return shader;
}

function createShaderProgram(gl, vertexSource, fragmentSource) {
  var program = gl.createProgram();

  var vertexShader = initializeShader(gl, vertexSource, gl.VERTEX_SHADER);
  var fragmentShader = initializeShader(gl, fragmentSource, gl.FRAGMENT_SHADER);

  console.log(vertexShader);

  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);

  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    console.log("Error linking program!", gl.getProgramInfoLog(program));
    return null;
  }

  gl.validateProgram(program) //Apparently this step should only be done in debug releases because it slows rendering down
  if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
    console.log("Error validating program", gl.getProgramInfoLog(program));
    return null;
  }

  return program;
}

function createVertexBuffer(gl, vertexData) {
  var buf = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buf);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexData), gl.STATIC_DRAW);
  return buf;
}

function createIndexBuffer(gl, indexData) {
  var buf = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buf);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexData), gl.STATIC_DRAW);
  return buf;
}

var TriangleMesh = function(gl, vertexPositions, normals, colors, indices, vertexSource, fragmentSource) {
  this.indexCount = indices.length;
  this.positionVbo = createVertexBuffer(gl, vertexPositions);
  this.colorVbo = createVertexBuffer(gl, colors);
  this.normalVbo = createVertexBuffer(gl, normals);
  this.indexIbo = createIndexBuffer(gl, indices);
  this.shaderProgram = createShaderProgram(gl, vertexSource, fragmentSource);
}

TriangleMesh.prototype.render = function(gl) {

  gl.useProgram(this.shaderProgram);

  gl.bindBuffer(gl.ARRAY_BUFFER, this.positionVbo);
  var positionAttribLocation = gl.getAttribLocation(this.shaderProgram, 'aVertPosition');
  gl.vertexAttribPointer(
    positionAttribLocation, //Attrib Location
    3, //Number of elems per attribute
    gl.FLOAT, //32 bit floats
    gl.FALSE, //Data not normalized
    3*Float32Array.BYTES_PER_ELEMENT,//Size of individual vertex
    0// Offset from the beginning of a single vertex to this attribute
  );
  gl.enableVertexAttribArray(positionAttribLocation);

  gl.bindBuffer(gl.ARRAY_BUFFER, this.colorVbo);
  var colorAttribLocation = gl.getAttribLocation(this.shaderProgram, 'aVertColor');
  gl.vertexAttribPointer(
    colorAttribLocation,
    3,
    gl.FLOAT,
    gl.FALSE,
    3*Float32Array.BYTES_PER_ELEMENT,
    0
  );
  gl.enableVertexAttribArray(colorAttribLocation);

  gl.bindBuffer(gl.ARRAY_BUFFER, this.normalVbo);
  var normalsAttribLocation = gl.getAttribLocation(this.shaderProgram, 'aVertNormals');

  gl.vertexAttribPointer(
    normalsAttribLocation,
    3,
    gl.FLOAT,
    gl.FALSE,
    3*Float32Array.BYTES_PER_ELEMENT,
    0
  );
  gl.enableVertexAttribArray(normalsAttribLocation);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexIbo);

  gl.useProgram(this.shaderProgram);

  //Set uniforms

  var matModelUniformLocation = gl.getUniformLocation(this.shaderProgram, 'mModel');
  var matViewUniformLocation = gl.getUniformLocation(this.shaderProgram, 'mView');
  var matProjUniformLocation = gl.getUniformLocation(this.shaderProgram, 'mProj');
  var matInvModelUniformLocation = gl.getUniformLocation(this.shaderProgram, 'mInvModel');
  var matInvViewUniformLocation = gl.getUniformLocation(this.shaderProgram, 'mInvView');

  var modelMatrix = new Float32Array(16);  //Init with all zeros
  var viewMatrix = new Float32Array(16);
  var projMatrix = new Float32Array(16);
  var invModelMatrix = new Float32Array(16);
  var invViewMatrix = new Float32Array(16);

  mat4.identity(modelMatrix); //Modify the values of modelMatrix
  mat4.lookAt(viewMatrix, [0, 0, -5], [0, 0, 0], [0, 1, 0]);
  mat4.perspective(projMatrix, glMatrix.toRadian(45), aspectRatio, 0.1, 1000.0);
  mat4.invert(invViewMatrix, viewMatrix);
  mat4.invert(invModelMatrix, modelMatrix);

  //Map uniforms to shader program values
  gl.uniformMatrix4fv(matModelUniformLocation, gl.FALSE, modelMatrix); //Set to true if you want it transposed but it doesn't work on webGL anyways so don't set it to gl.TRUE
  gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
  gl.uniformMatrix4fv(matProjUniformLocation, gl.FALSE, projMatrix);
  gl.uniformMatrix4fv(matInvModelUniformLocation, gl.FALSE, invModelMatrix);
  gl.uniformMatrix4fv(matInvViewUniformLocation, gl.FALSE, invViewMatrix);

  //Main render loop
  var identityMatrix = new Float32Array(16);
  mat4.identity(identityMatrix);
  var angle = 0; //Set angle here because memory allocation can take a while in a loop

  var xRotationMatrix = new Float32Array(16);
  var yRotationMatrix = new Float32Array(16);

  var rotateX = Math.floor(Math.random()*4 + 1);
  var rotateY = Math.floor(Math.random()*4 + 1);

  var indexCount = this.indexCount;

  var loop = function() {
    angle = performance.now() / 1000 / 8 * 2 * Math.PI; //Full rotation every 8 seconds

    mat4.rotate(yRotationMatrix, identityMatrix, angle / rotateX, [0, 1, 0]);
    mat4.rotate(xRotationMatrix, identityMatrix, angle / rotateY, [1, 0, 0]);
    mat4.mul(modelMatrix, yRotationMatrix, xRotationMatrix);
    gl.uniformMatrix4fv(matModelUniformLocation, gl.FALSE, modelMatrix);

    gl.clearColor(0.0, 0.0, 0.1, 1.0);
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

    gl.drawElements(gl.TRIANGLES, indexCount, gl.UNSIGNED_SHORT, 0); //Draw using triangles, skip 0,
    requestAnimationFrame(loop); //Won't draw when no one is looking at it -- other tab
  }
  requestAnimationFrame(loop);

}

var InitDemo = function() {

  var gl = initializeWebGL();

  console.log("GL value is: ");
  console.log(gl);

  var myWebGLTask = new DrawMeshesTask(gl);
  myWebGLTask.render(gl);

}

var DrawMeshesTask = function(gl) {
  this.cameraAngle = 0;
  this.meshArray = [];

  this.meshArray.push(new TriangleMesh(gl, sphereVertices, sphereNormals, sphereColors, sphereIndices, vertexSource, fragmentSource));
  //this.meshArray.push(new TriangleMesh(gl, cubeVertices, cubeNormals, cubeColors, cubeIndices, vertexSource, fragmentSource));
}

DrawMeshesTask.prototype.render = function(gl) {
  this.meshArray.forEach(function(mesh) {
    console.log("IndexCount is: ");
    console.log(mesh.indexCount);
    mesh.render(gl);
  });
}

DrawMeshesTask.prototype.dragCamera = function(dy) {
  this.cameraAngle = Math.min(Math.max(this.cameraAngle + dy*0.5, -90), 90);
}
